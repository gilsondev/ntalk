module.exports = () => {
  const HomeController = {
    index: (req, res) => res.render('home/index', { title: 'Express' }),
  };

  return HomeController;
};
