module.exports = {
	"extends": "airbnb-base",
	"env": 	{
		"mocha": true
	},
	"rules": {
		"no-console": 0,
		"no-unused-vars": ["error", {
			"argsIgnorePattern": "next"
		}]
	}
};