const app = require('../app-test');

describe('No controller home', () => {
  it('GET / retorna status 200', (done) => {
    app.get('/').expect(200).end(() => done());
  });
});
